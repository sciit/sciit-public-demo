
Feature: Photo upload for claims
Scenario: Small JPEG Upload with Description
 Given a claim
 And a small JPEG 
 And a description
 When I select the photograph
 And I enter a description
 And I click submit
 Then the photograph is stored
 And a database entry is created.

